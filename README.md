Para testar este sistema, por favor, instale o node, o python3, o pip3, e depois rode na pasta raíz: 

```
$ npm i 
```

para as dependências do node, e:

```
$ pip3 install python-socketio
```

para a dependência do python.