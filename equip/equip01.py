import socketio
import equip_pb2
from random import randrange



# standard Python
sio = socketio.Client()

equip = equip_pb2.Equip()
equip.name = 'Ar Condicionado'
equip.state = True
equip.data = randrange(15, 22)



@sio.event
def connect():
    print('Estou conectado!')

@sio.event
def disconnect():
    print("Estou desconectado!")


@sio.on('changeTemp')
def on_message(data):
    equip.data = data
    sio.emit('equipConnected', equip.SerializeToString())

@sio.on('changeStateAr')
def on_message(data):
    equip.state = data
    sio.emit('equipConnected', equip.SerializeToString())

sio.connect('http://localhost:8000')

sio.emit('equipConnected', equip.SerializeToString())




