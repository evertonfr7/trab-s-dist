import socketio
import equip_pb2



# standard Python
sio = socketio.Client()

equip = equip_pb2.Equip()
equip.name = 'TV'
equip.state = True
equip.data = 10



@sio.event
def connect():
    print('Estou conectado!')

@sio.event
def disconnect():
    print("Estou desconectado!")

@sio.on('changeChannel')
def on_message(data):
    equip.data = data
    sio.emit('equipConnected', equip.SerializeToString())

@sio.on('changeStateTV')
def on_message(data):
    equip.state = data
    sio.emit('equipConnected', equip.SerializeToString())


sio.connect('http://localhost:8000')

sio.emit('equipConnected', equip.SerializeToString())


sio.emit('equipDisconnected', equip.SerializeToString())




