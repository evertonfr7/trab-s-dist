import socketio
import equip_pb2
from random import randrange



# standard Python
sio = socketio.Client()

equip = equip_pb2.Equip()
equip.name = 'Lampada'
equip.state = True
equip.data = 0



@sio.event
def connect():
    print('Estou conectado!')

@sio.event
def disconnect():
    print("Estou desconectado!")

@sio.on('changeStateLamp')
def on_message(data):
    equip.state = data
    sio.emit('equipConnected', equip.SerializeToString())

sio.connect('http://localhost:8000')

sio.emit('equipConnected', equip.SerializeToString())




