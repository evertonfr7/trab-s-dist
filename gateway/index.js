const equips = require('../protobuf/equip_pb');
const pikachu = require('../pikachu/pikachu');
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);


let clients = 0;


app.get('/', function(req, res){
    res.send('<h1>Open!</h1>');
  });


io.on("connection", (socket) => {
    
    socket.on('clientConnected', (data) => {
        clients++;
        console.log(`${data} id:${socket.id}`);
    }); 

    socket.on("disconnect", () => {
        clients--;
        console.log(`id: ${socket.id} está desconectado(a).`);
    });
    
    
    socket.on('equipConnected', (data) => {
        let equip = equips.Equip.deserializeBinary(data);
        if(equip.getName() == 'TV'){
            console.log(`Nome: ${equip.getName()}, Canal: ${equip.getData()}, Ativado: ${equip.getState()}, id: ${socket.id}`);     
        }
        else if(equip.getName() == 'Lampada'){
            console.log(`Nome: ${equip.getName()}, Ativado: ${equip.getState()}, id: ${socket.id}`);     
        }
        else{
            console.log(`Nome: ${equip.getName()}, Temperatura: ${equip.getData()}, Ativado: ${equip.getState()}, id: ${socket.id}`); 
        }
        io.emit('sendEquipConnected', data);
    });

   

    socket.on('setTemp', (data) => {
        io.emit('changeTemp', data);
        io.send('changeTemp', data);
    });

    socket.on('setChannel', (data) => {
        io.emit('changeChannel', data);
        io.send('changeChannel', data);
    });

    socket.on('setStateTV', (data) => {
        io.emit('changeStateTV', data);
        io.send('changeStateTV', data);
    });

    socket.on('setStateLamp', (data) => {
        io.emit('changeStateLamp', data);
        io.send('changeStateLamp', data);
    });


    socket.on('setStateAr', (data) => {
        io.emit('changeStateAr', data);
        io.send('changeStateAr', data);
    });


    
});




http.listen(process.env.PORT || 1234, function() {
    if(!clients){
        pikachu;
    }
});


