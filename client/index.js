const clients = require('../protobuf/client_pb');
const equips = require('../protobuf/equip_pb');
const io = require("socket.io-client");
const socket = io("http://localhost:8000");

var client = new clients.Client();


client.setName("Lulula");

socket.on("connect", () => {
    console.log(`id: ${client.getName()} está conectado(a).`);
    socket.emit('clientConnected', `${client.getName()} está conectado(a).`);
});

socket.on("disconnect", () => {
    console.log(`id: ${client.getName()} está desconectado(a).`);
});


socket.on('sendEquipConnected', (data) => {
    let equip = equips.Equip.deserializeBinary(data);
    if(equip.getName() == 'TV'){
        console.log(`Nome: ${equip.getName()}, Canal: ${equip.getData()}, Ativado: ${equip.getState()}, id: ${socket.id}`);     
    }
    else if(equip.getName() == 'Lampada'){
        console.log(`Nome: ${equip.getName()}, Ativado: ${equip.getState()}, id: ${socket.id}`);     
    }
    else{
        console.log(`Nome: ${equip.getName()}, Temperatura: ${equip.getData()}, Ativado: ${equip.getState()}, id: ${socket.id}`); 
    }
});




function setTemp(temp){
    socket.emit('setTemp', temp);
}

function setChannel(channel){
    socket.emit('setChannel', channel);
}

function setStateTV(state){
    socket.emit('setStateTV', state);
}

function setStateLamp(state){
    socket.emit('setStateLamp', state);
}

function setStateAr(state){
    socket.emit('setStateAr', state);
}

setTimeout(() => {
    setTemp(10);
}, 10000);


setTimeout(() => {
    setChannel(8);
}, 20000);


setTimeout(() => {
    setStateLamp(false);
}, 30000);

setTimeout(() => {
    setStateAr(false);
}, 40000);

setTimeout(() => {
    setStateTV(false);
}, 50000);


setTimeout(() => {
    setTemp(15);
    setChannel(10);
    setStateLamp(true);
    setStateAr(true);
    setStateTV(true);
}, 60000);

